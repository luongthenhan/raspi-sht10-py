# Demo nhận dữ liệu nhiệt độ và độ ẩm bằng sensor SHT10 với code Python #

Cấu hình
---

Sử dụng sensor SHT10 (185k):

* Chân + cắm vào điện áp dương 5VDC (Pin2 hoặc Pin4)
* Chân - cắm vào Ground (Pin9)
* Chân S cắm vào GPCLK0 (GPIO 7) (Pin7)
* Chân D cắm vào GPIO 0 (Pin11)

Dùng thư viện Sht1x ở đường dẫn: https://pypi.python.org/pypi/rpiSht1x/1.2

Chạy chương trình
---

  > sudo apt-get install python-setuptools

  > sudo easy_install web.py

  > sudo easy_install --upgrade pytz
  
  > sudo python sensor_ws.py