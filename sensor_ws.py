#!/usr/bin/env python
import web
from sensor import WaitingSht1x
#from lcd import Lcd
from datetime import datetime
from pytz import timezone
import json

urls = (
    '/sensor/get', 'TemperatureAndHumidity'
)

app = web.application(urls, globals())

#LCD_LINE_1 = 0x80 # LCD RAM address for the 1st line
#LCD_LINE_2 = 0xC0 # LCD RAM address for the 2nd line


class TemperatureAndHumidity:

    walker = 0

    def __init__(self):
        self.sensor = WaitingSht1x(11, 7)

    def GET(self):
        web.header('Access-Control-Allow-Origin', '*')
        aTouple = self.sensor.read_temperature_and_Humidity()
        print("Temperature: {} Humidity: {}".format(aTouple[0], aTouple[1]))
        now = datetime.now(timezone('Asia/Ho_Chi_Minh')).strftime('%Y-%m-%d %H:%M:%S')
        #Lcd.lcd_init()
        #Lcd.display(now, LCD_LINE_1)
        #Lcd.display("{0:.2f}*C - {1:.2f}%".format(aTouple[0], aTouple[1]), LCD_LINE_2)
        with open("data.txt", "a") as text_file:
            text_file.write(now + " : {0:.2f}*C - {1:.2f}%".format(aTouple[0], aTouple[1]))
            text_file.write("\n")
        TemperatureAndHumidity.walker += 1
        return json.dumps({"id" : TemperatureAndHumidity.walker, "time" : now, "temperature" : float(format(aTouple[0], '.2f')), "humidity" : float(format(aTouple[1], '.2f'))})

if __name__ == "__main__":
    app.run()